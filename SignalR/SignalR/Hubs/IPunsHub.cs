﻿namespace SignalR.Hubs
{
    public interface IPunsHub
    {
        void DrawPath(string path);
        void Clear();
        void DrawRequest();
    }
}
