﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace SignalR.Hubs
{
    [HubName("puns")]
    public class PunsHub : Hub<IPunsHub>
    {
        static List<string> m_cached_path = new List<string>();
        public void SendPath(string path)
        {
            m_cached_path.Add(path);
            Clients.All.DrawPath(path);
        }
        public void SendClearRequest()
        {
            m_cached_path.Clear();
            Clients.Others.Clear();
        }
        public override Task OnConnected()
        {
           
            return base.OnConnected();
        }
        public void DrawRequest()
        {
            foreach (var item in m_cached_path)
            {
                Clients.Caller.DrawPath(item);
            }

        }
    }
}