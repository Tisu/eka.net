﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace SignalR.Hubs
{
    [HubName("chat")]
    public class ChatHub : Hub<IChatHub>
    {
        public void SendMessage(string message)
        {
            Clients.All.SendMessageToAll(message);
        }
    }
}

public interface IChatHub
{
    void SendMessageToAll(string message);
}