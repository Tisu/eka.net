using System.Collections.Generic;
using System.Security.Claims;
using System.Web.Http;
using YellowNotes.Api.Providers;
using YellowNotes.Dto;

namespace YellowNotes.Api.Controllers
{
    public abstract class NotesControllerBase : ApiController
    {
        ClaimsIdentity _identity;
        public NotesControllerBase()
        {
            _identity  = RequestContext.Principal.Identity as ClaimsIdentity;
        }
        protected string Device => _identity.IsAuthenticated ? _identity.FindFirst(ApiConstants.ClaimDevice).Value : string.Empty;
        protected static readonly Dictionary<int, NoteDto> Notes =
            new Dictionary<int, NoteDto>()
            {
                {1, new NoteDto {Id = 1, Title = "Title 1", Content = "Content 1",Created = "Db"}},
                {2, new NoteDto {Id = 2, Title = "Title 2", Content = "Content 2",Created = "Db"}},
            };
    }
}