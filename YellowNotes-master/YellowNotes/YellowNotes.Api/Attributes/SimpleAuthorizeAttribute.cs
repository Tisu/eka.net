﻿

using System;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Controllers;
using YellowNotes.Api.Providers;

namespace YellowNotes.Api.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class SimpleAuthorizeAttribute : AuthorizeAttribute
    {
        public string Devices { get; set; }
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var baseIsAuthorized = base.IsAuthorized(actionContext);
            if (!baseIsAuthorized)
            {
                return false;
            }
            var simpleAuthorizeAttribute = actionContext.ActionDescriptor.GetCustomAttributes<SimpleAuthorizeAttribute>(true).FirstOrDefault() ??
                actionContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes<SimpleAuthorizeAttribute>(true).FirstOrDefault();
            if (simpleAuthorizeAttribute == null)
            {
                return true;
            }
            var allowedDevices = simpleAuthorizeAttribute.Devices.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries );
            if (allowedDevices.Length < 1)
            {
                return true;
            }
            var identity = actionContext.RequestContext.Principal.Identity as ClaimsIdentity;
            var device = identity.FindFirst(ApiConstants.ClaimDevice).Value;

            return allowedDevices.Contains(device);
            
        }
    }
}