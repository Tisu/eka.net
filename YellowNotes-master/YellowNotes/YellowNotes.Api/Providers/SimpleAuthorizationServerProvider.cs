﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using YellowNotes.Api.Providers;
using YellowNotes.Api.Services;

namespace YellowNotes.Api.App_Start
{
    internal class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public string ClaimsType { get; private set; }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            var device_id = context.Parameters.Get("device");
            context.OwinContext.Set("device", device_id);
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            var device = context.OwinContext.Get<string>("device");
            if (string.IsNullOrEmpty(device))
            {
                context.SetError("invalid_device", "Device id required");
                return;
            }
            var service = new UsersService();
            var user = service.GetUser(context.UserName);

            if (user == null || user.Password != context.Password)
            {
                context.SetError("User invalid");
                return;
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
            identity.AddClaim(new Claim(ApiConstants.ClaimDevice, device));

            var ticket = new AuthenticationTicket(identity, new AuthenticationProperties(new Dictionary<string, string>()
            {
                ["fullname"] = user.FullName,


            }
            ));
            context.Validated(identity);

        }
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }