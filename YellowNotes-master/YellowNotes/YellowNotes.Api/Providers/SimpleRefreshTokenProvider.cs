﻿using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using YellowNotes.Api.Services;
using YellowNotes.Dto;

namespace YellowNotes.Api.Providers
{
    public class SimpleRefreshTokenProvider : AuthenticationTokenProvider
    {
        private readonly TokenService _token_service = new TokenService();
        public override async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var refreshTokenId = Guid.NewGuid().ToString("n");

            var token = new RefreshTokenDto
            {
                UserName = context.Ticket.Identity.Name,
                Token = GetHash(refreshTokenId),

                IssuedDate = DateTime.UtcNow,
                ExpirationDate = DateTime.UtcNow.AddMinutes(Convert.ToDouble(10))
            };

            context.Ticket.Properties.IssuedUtc = token.IssuedDate;
            context.Ticket.Properties.ExpiresUtc = token.ExpirationDate;

            token.ProtectedTicket = context.SerializeTicket();

            _token_service.Save(token);
            context.SetToken(refreshTokenId);
        }
        public override async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            string hashedTokenId = GetHash(context.Token);
            var token = _token_service.Find(context.Token);
            if (token != null)
            {
                context.DeserializeTicket(token.ProtectedTicket);
                _token_service.Remove(token);
            }
        }
        private static string GetHash(string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();

            byte[] byteValue = System.Text.Encoding.UTF8.GetBytes(input);

            byte[] byteHash = hashAlgorithm.ComputeHash(byteValue);

            return Convert.ToBase64String(byteHash);
        }
    }

}
