﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YellowNotes.Dto;

namespace YellowNotes.Api.Services
{
    public class TokenService
    {
        private static List<RefreshTokenDto> Tokens = new List<RefreshTokenDto>();

        internal void Save(RefreshTokenDto token)
        {
            Tokens.Add(token);
        }

        internal RefreshTokenDto Find(string hashedId)
        {
            return Tokens.FirstOrDefault(x => x.Token == hashedId);
        }

        internal void Remove(RefreshTokenDto token)
        {
            Tokens.Remove(token);
        }
    }
}