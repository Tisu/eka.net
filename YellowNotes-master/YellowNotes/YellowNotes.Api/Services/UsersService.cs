﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YellowNotes.Dto;

namespace YellowNotes.Api.Services
{
    public class UsersService
    {
        private static Dictionary<string, UserDto> Users = new Dictionary<string, UserDto>
        {
            ["lkurzyniec"] = new UserDto() { UserName = "lkurzyniec" , LastName = "dsadsa",FirstName= " dsada",Password="zdupy"},
            ["jkowalski"] = new UserDto() { UserName = "jkowalski", LastName = "dsadsa", FirstName = " dsada",Password="sdadsa" },
            ["test"] = new UserDto() { UserName = "Piotr", LastName = "dsadsa", FirstName = " dsada",Password ="dsada" },
            
        };

        internal UserDto GetUser(string name)
        {
            UserDto user;
            Users.TryGetValue(name, out user);
            return user;
        }
    }
}