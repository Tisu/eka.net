﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YellowNotes.Dto
{
    public class RefreshTokenDto
    {
        public string UserName { get; set; }
        public string Token { get; set; }
        public DateTime IssuedDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string ProtectedTicket { get; set; }
    }
}
